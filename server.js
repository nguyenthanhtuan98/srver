
const express = require('express')
const app = express()
const mongoose = require('mongoose');
const bodyParser = require("body-parser");
var cors = require('cors');

const authRT = require('./router/auth.js')
const productRT = require('./router/products.js')


app.use(cors())

//body-parse
app.use(bodyParser.json({ limit: "10000kb" })); // for parsing application/json
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

require('dotenv').config()
const port = process.env.PORT

// CONNECT MONGDB
const url = process.env.MONGO_URL;
const userhost = process.env.DB_USER;
const option = {}
mongoose.connect(`${userhost}${url}`, option)
.then(() => console.log('MongoDB connection established.'))
.catch((error) => console.error("MongoDB connection failed:", error.message))

app.use('/auth', authRT)
app.use('/products', productRT)

 

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})