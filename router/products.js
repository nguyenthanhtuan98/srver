const express = require('express');
const controller = require('../controller/products.js')




const router = express.Router();
router.post('/save', controller.save)
router.get("/list", controller.list)
router.post("/delete", controller.delete)
router.get("/views/:id", controller.views)



module.exports = router;